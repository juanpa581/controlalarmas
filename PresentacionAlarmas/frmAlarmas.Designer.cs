﻿
namespace PresentacionAlarmas
{
    partial class frmAlarmas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCapturaDeAlarmas = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.txtBuscarEstadoAlarma = new System.Windows.Forms.TextBox();
            this.lblBuscarEstado = new System.Windows.Forms.Label();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.dtgCapturaAlarmas = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCapturaAlarmas)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCapturaDeAlarmas
            // 
            this.lblCapturaDeAlarmas.AutoSize = true;
            this.lblCapturaDeAlarmas.BackColor = System.Drawing.Color.Transparent;
            this.lblCapturaDeAlarmas.Location = new System.Drawing.Point(476, 62);
            this.lblCapturaDeAlarmas.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lblCapturaDeAlarmas.Name = "lblCapturaDeAlarmas";
            this.lblCapturaDeAlarmas.Size = new System.Drawing.Size(147, 18);
            this.lblCapturaDeAlarmas.TabIndex = 81;
            this.lblCapturaDeAlarmas.Text = "Captura de Alarmas";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox1.Location = new System.Drawing.Point(88, 22);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1028, 97);
            this.pictureBox1.TabIndex = 80;
            this.pictureBox1.TabStop = false;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(990, 140);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 79;
            this.btnAgregar.Text = "+";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtBuscarEstadoAlarma
            // 
            this.txtBuscarEstadoAlarma.Location = new System.Drawing.Point(319, 142);
            this.txtBuscarEstadoAlarma.Margin = new System.Windows.Forms.Padding(4);
            this.txtBuscarEstadoAlarma.Name = "txtBuscarEstadoAlarma";
            this.txtBuscarEstadoAlarma.Size = new System.Drawing.Size(501, 26);
            this.txtBuscarEstadoAlarma.TabIndex = 78;
            this.txtBuscarEstadoAlarma.TextChanged += new System.EventHandler(this.txtBuscarEstadoAlarma_TextChanged);
            // 
            // lblBuscarEstado
            // 
            this.lblBuscarEstado.AutoSize = true;
            this.lblBuscarEstado.Location = new System.Drawing.Point(85, 142);
            this.lblBuscarEstado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBuscarEstado.Name = "lblBuscarEstado";
            this.lblBuscarEstado.Size = new System.Drawing.Size(186, 18);
            this.lblBuscarEstado.TabIndex = 77;
            this.lblBuscarEstado.Text = "Buscar Estado de Alarma";
            // 
            // btnRegresar
            // 
            this.btnRegresar.Location = new System.Drawing.Point(953, 568);
            this.btnRegresar.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(112, 32);
            this.btnRegresar.TabIndex = 76;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(566, 568);
            this.btnModificar.Margin = new System.Windows.Forms.Padding(4);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(112, 32);
            this.btnModificar.TabIndex = 75;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(88, 568);
            this.btnBorrar.Margin = new System.Windows.Forms.Padding(4);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(112, 32);
            this.btnBorrar.TabIndex = 74;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // dtgCapturaAlarmas
            // 
            this.dtgCapturaAlarmas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCapturaAlarmas.Location = new System.Drawing.Point(88, 193);
            this.dtgCapturaAlarmas.Margin = new System.Windows.Forms.Padding(4);
            this.dtgCapturaAlarmas.Name = "dtgCapturaAlarmas";
            this.dtgCapturaAlarmas.Size = new System.Drawing.Size(1028, 316);
            this.dtgCapturaAlarmas.TabIndex = 73;
            this.dtgCapturaAlarmas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgCapturaAlarmas_CellClick);
            // 
            // frmAlarmas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1200, 623);
            this.Controls.Add(this.lblCapturaDeAlarmas);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtBuscarEstadoAlarma);
            this.Controls.Add(this.lblBuscarEstado);
            this.Controls.Add(this.btnRegresar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.dtgCapturaAlarmas);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmAlarmas";
            this.Text = "frmAlarmas";
            this.Load += new System.EventHandler(this.frmAlarmas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCapturaAlarmas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCapturaDeAlarmas;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.TextBox txtBuscarEstadoAlarma;
        private System.Windows.Forms.Label lblBuscarEstado;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.DataGridView dtgCapturaAlarmas;
    }
}