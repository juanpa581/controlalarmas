﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresAlarma;
using EntidadesAlarma;

namespace PresentacionAlarmas
{
    public partial class frmAlarmasAdd : Form
    {
        ManejadorAlarma ma = new ManejadorAlarma();
        Alarma a;
        int id = 0;
        public frmAlarmasAdd()
        {
            InitializeComponent();
        }
        public frmAlarmasAdd(Alarma alarma)
        {
            InitializeComponent();
            id = alarma.IdAlarma;
            txtEstado.Text = alarma.Estado;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (id > 0)
            {
                a = new Alarma(id, txtEstado.Text);
                string r = ma.Modificar(a);
                Close();
            }
            else
                try
                {
                    string resultado = ma.Guardar(new Alarma(0, txtEstado.Text));
                    Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Error de dato");
                    txtEstado.Focus();
                }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
