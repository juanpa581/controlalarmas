﻿
namespace PresentacionAlarmas
{
    partial class frmPrivilegios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCapturaDePrivilegios = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.txtBuscarPrivilegio = new System.Windows.Forms.TextBox();
            this.lblBuscarPrivilegio = new System.Windows.Forms.Label();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.dtgCapturaPrivilegios = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCapturaPrivilegios)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCapturaDePrivilegios
            // 
            this.lblCapturaDePrivilegios.AutoSize = true;
            this.lblCapturaDePrivilegios.BackColor = System.Drawing.Color.Transparent;
            this.lblCapturaDePrivilegios.Location = new System.Drawing.Point(476, 62);
            this.lblCapturaDePrivilegios.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lblCapturaDePrivilegios.Name = "lblCapturaDePrivilegios";
            this.lblCapturaDePrivilegios.Size = new System.Drawing.Size(163, 18);
            this.lblCapturaDePrivilegios.TabIndex = 90;
            this.lblCapturaDePrivilegios.Text = "Captura de Privilegios";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox1.Location = new System.Drawing.Point(88, 22);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1028, 97);
            this.pictureBox1.TabIndex = 89;
            this.pictureBox1.TabStop = false;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(990, 140);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 88;
            this.btnAgregar.Text = "+";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txtBuscarPrivilegio
            // 
            this.txtBuscarPrivilegio.Location = new System.Drawing.Point(319, 142);
            this.txtBuscarPrivilegio.Margin = new System.Windows.Forms.Padding(4);
            this.txtBuscarPrivilegio.Name = "txtBuscarPrivilegio";
            this.txtBuscarPrivilegio.Size = new System.Drawing.Size(501, 26);
            this.txtBuscarPrivilegio.TabIndex = 87;
            this.txtBuscarPrivilegio.TextChanged += new System.EventHandler(this.txtBuscarPrivilegio_TextChanged);
            // 
            // lblBuscarPrivilegio
            // 
            this.lblBuscarPrivilegio.AutoSize = true;
            this.lblBuscarPrivilegio.Location = new System.Drawing.Point(85, 142);
            this.lblBuscarPrivilegio.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBuscarPrivilegio.Name = "lblBuscarPrivilegio";
            this.lblBuscarPrivilegio.Size = new System.Drawing.Size(126, 18);
            this.lblBuscarPrivilegio.TabIndex = 86;
            this.lblBuscarPrivilegio.Text = "Buscar Privilegio";
            // 
            // btnRegresar
            // 
            this.btnRegresar.Location = new System.Drawing.Point(953, 568);
            this.btnRegresar.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(112, 32);
            this.btnRegresar.TabIndex = 85;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(566, 568);
            this.btnModificar.Margin = new System.Windows.Forms.Padding(4);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(112, 32);
            this.btnModificar.TabIndex = 84;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(88, 568);
            this.btnBorrar.Margin = new System.Windows.Forms.Padding(4);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(112, 32);
            this.btnBorrar.TabIndex = 83;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // dtgCapturaPrivilegios
            // 
            this.dtgCapturaPrivilegios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCapturaPrivilegios.Location = new System.Drawing.Point(88, 193);
            this.dtgCapturaPrivilegios.Margin = new System.Windows.Forms.Padding(4);
            this.dtgCapturaPrivilegios.Name = "dtgCapturaPrivilegios";
            this.dtgCapturaPrivilegios.Size = new System.Drawing.Size(1028, 316);
            this.dtgCapturaPrivilegios.TabIndex = 82;
            this.dtgCapturaPrivilegios.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgCapturaPrivilegios_CellClick);
            // 
            // frmPrivilegios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 623);
            this.Controls.Add(this.lblCapturaDePrivilegios);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtBuscarPrivilegio);
            this.Controls.Add(this.lblBuscarPrivilegio);
            this.Controls.Add(this.btnRegresar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.dtgCapturaPrivilegios);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmPrivilegios";
            this.Text = "frmPrivilegios";
            this.Load += new System.EventHandler(this.frmPrivilegios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCapturaPrivilegios)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCapturaDePrivilegios;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.TextBox txtBuscarPrivilegio;
        private System.Windows.Forms.Label lblBuscarPrivilegio;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.DataGridView dtgCapturaPrivilegios;
    }
}