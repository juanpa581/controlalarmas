﻿
namespace PresentacionAlarmas
{
    partial class frmMenu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbAlarma = new System.Windows.Forms.ToolStripButton();
            this.tsbPrivilegios = new System.Windows.Forms.ToolStripButton();
            this.tsbUsuarios = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAlarma,
            this.tsbPrivilegios,
            this.tsbUsuarios});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 71);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbAlarma
            // 
            this.tsbAlarma.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAlarma.Image = ((System.Drawing.Image)(resources.GetObject("tsbAlarma.Image")));
            this.tsbAlarma.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAlarma.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAlarma.Name = "tsbAlarma";
            this.tsbAlarma.Size = new System.Drawing.Size(68, 68);
            this.tsbAlarma.Text = "Alarma";
            this.tsbAlarma.Click += new System.EventHandler(this.tsbAlarma_Click);
            // 
            // tsbPrivilegios
            // 
            this.tsbPrivilegios.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPrivilegios.Image = ((System.Drawing.Image)(resources.GetObject("tsbPrivilegios.Image")));
            this.tsbPrivilegios.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbPrivilegios.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPrivilegios.Name = "tsbPrivilegios";
            this.tsbPrivilegios.Size = new System.Drawing.Size(68, 68);
            this.tsbPrivilegios.Text = "Privilegios";
            this.tsbPrivilegios.Click += new System.EventHandler(this.tsbPrivilegios_Click);
            // 
            // tsbUsuarios
            // 
            this.tsbUsuarios.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbUsuarios.Image = ((System.Drawing.Image)(resources.GetObject("tsbUsuarios.Image")));
            this.tsbUsuarios.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbUsuarios.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbUsuarios.Name = "tsbUsuarios";
            this.tsbUsuarios.Size = new System.Drawing.Size(68, 68);
            this.tsbUsuarios.Text = "Usuarios";
            this.tsbUsuarios.Click += new System.EventHandler(this.tsbUsuarios_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStrip1);
            this.IsMdiContainer = true;
            this.Name = "frmMenu";
            this.Text = "Form1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbAlarma;
        private System.Windows.Forms.ToolStripButton tsbPrivilegios;
        private System.Windows.Forms.ToolStripButton tsbUsuarios;
    }
}

