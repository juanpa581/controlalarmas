﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresAlarma;
using EntidadesAlarma;

namespace PresentacionAlarmas
{
    public partial class frmPrivilegiosAdd : Form
    {
        ManejadorPrivilegios mp = new ManejadorPrivilegios();
        Privilegios p;
        int id = 0;
        public frmPrivilegiosAdd()
        {
            InitializeComponent();
        }
        public frmPrivilegiosAdd(Privilegios privilegios)
        {
            InitializeComponent();
            id = privilegios.IdPrivilegio;
            txtPrivilegio.Text = privilegios.Privilegio;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (id > 0)
            {
                p = new Privilegios(id, txtPrivilegio.Text);
                string r = mp.Modificar(p);
                Close();
            }
            else
                try
                {
                    string resultado = mp.Guardar(new Privilegios(0, txtPrivilegio.Text));
                    Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Error de dato");
                    txtPrivilegio.Focus();
                }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
