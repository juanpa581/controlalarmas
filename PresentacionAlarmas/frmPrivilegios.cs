﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresAlarma;
using EntidadesAlarma;

namespace PresentacionAlarmas
{
    public partial class frmPrivilegios : Form
    {
        ManejadorPrivilegios mp;
        Privilegios p = new Privilegios(0,"");
        int fila = 0;
        string r = "";
        public frmPrivilegios()
        {
            InitializeComponent();
            mp = new ManejadorPrivilegios();
        }
        void Actualizar()
        {
            dtgCapturaPrivilegios.DataSource = mp.Listado(string.Format("" +
                "Select * from Privilegios where Privilegio like '%{0}%'", txtBuscarPrivilegio.Text), "Privilegios").Tables[0];
            dtgCapturaPrivilegios.AutoResizeColumns();
            dtgCapturaPrivilegios.Columns[0].ReadOnly = true;
        }

        private void frmPrivilegios_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmPrivilegiosAdd fpa = new frmPrivilegiosAdd();
            fpa.ShowDialog();
            Actualizar();
        }

        private void txtBuscarPrivilegio_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dtgCapturaPrivilegios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            p.IdPrivilegio = int.Parse(dtgCapturaPrivilegios.Rows[fila].Cells[0].Value.ToString());
            p.Privilegio = dtgCapturaPrivilegios.Rows[fila].Cells[1].Value.ToString();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atención está seguro de borrar?" + 
                p.Privilegio, "|Atención|", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = mp.Borrar(p);
                Actualizar();
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            frmPrivilegiosAdd fpa = new frmPrivilegiosAdd(p);
            fpa.ShowDialog();
            Actualizar();
        }
    }
}
