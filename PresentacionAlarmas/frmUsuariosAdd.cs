﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresAlarma;
using EntidadesAlarma;

namespace PresentacionAlarmas
{
    public partial class frmUsuariosAdd : Form
    {
        ManejadorUsuarios mu = new ManejadorUsuarios();
        Usuarios u;
        int id = 0;
        public frmUsuariosAdd()
        {
            InitializeComponent();
        }
        public frmUsuariosAdd(Usuarios usuarios)
        {
            InitializeComponent();
            id = usuarios.IdUsuario;
            txtNombre.Text = usuarios.Nombre;
            txtApellidoPaterno.Text = usuarios.ApellidoPaterno;
            txtApellidoMaterno.Text = usuarios.ApellidoMaterno;
            txtContraseña.Text = usuarios.Contraseña;
            txtFkIdPrivilegio.Text = usuarios.FkIdPrivilegio.ToString();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (id > 0)
            {
                u = new Usuarios(id, txtNombre.Text, txtApellidoPaterno.Text, txtApellidoMaterno.Text, txtContraseña.Text, 
                    int.Parse(txtFkIdPrivilegio.Text));
                string r = mu.Modificar(u);
                Close();
            }
            else
                try
                {
                    string resultado = mu.Guardar(new Usuarios(0, txtNombre.Text, txtApellidoPaterno.Text, 
                        txtApellidoMaterno.Text, txtContraseña.Text, int.Parse(txtFkIdPrivilegio.Text)));
                    Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Error de dato");
                    txtNombre.Focus();
                }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
