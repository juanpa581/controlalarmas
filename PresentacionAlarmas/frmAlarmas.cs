﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ManejadoresAlarma;
using AccesoDatosAlarma;
using EntidadesAlarma;

namespace PresentacionAlarmas
{
    public partial class frmAlarmas : Form
    {
        ManejadorAlarma ma;
        Alarma a = new Alarma(0,"");
        int fila = 0;
        string r = "";
        public frmAlarmas()
        {
            InitializeComponent();
            ma = new ManejadorAlarma();
        }
        void Actualizar()
        {
            dtgCapturaAlarmas.DataSource = ma.Listado(string.Format("" +
                "Select * from Alarma where Estado like '%{0}%'", txtBuscarEstadoAlarma.Text), "Alarma").Tables[0];
            dtgCapturaAlarmas.AutoResizeColumns();
            dtgCapturaAlarmas.Columns[0].ReadOnly = true;
        }

        private void frmAlarmas_Load(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            frmAlarmasAdd faa = new frmAlarmasAdd();
            faa.ShowDialog();
            Actualizar();
        }

        private void txtBuscarEstadoAlarma_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dtgCapturaAlarmas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            a.IdAlarma = int.Parse(dtgCapturaAlarmas.Rows[fila].Cells[0].Value.ToString());
            a.Estado = dtgCapturaAlarmas.Rows[fila].Cells[1].Value.ToString();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atención está seguro de borrar?" + a.Estado, "|Atención|", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                r = ma.Borrar(a);
                Actualizar();
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            frmAlarmasAdd faa = new frmAlarmasAdd(a);
            faa.ShowDialog();
            Actualizar();
        }
    }
}
