﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentacionAlarmas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void tsbAlarma_Click(object sender, EventArgs e)
        {
            frmAlarmas a = new frmAlarmas();
            a.ShowDialog();
        }

        private void tsbPrivilegios_Click(object sender, EventArgs e)
        {
            frmPrivilegios p = new frmPrivilegios();
                p.ShowDialog();
        }

        private void tsbUsuarios_Click(object sender, EventArgs e)
        {
            frmUsuarios u = new frmUsuarios();
            u.ShowDialog();
        }
    }
}
