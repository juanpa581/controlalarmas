drop database if exists Alarmas;
create database Alarmas;
use Alarmas;

create table Privilegios(
IdPrivilegio int primary key auto_increment,
Privilegio varchar(100));

create table Alarma(
IdAlarma int primary key auto_increment,
Estado varchar(100));

create table Usuarios(
IdUsuario int primary key auto_increment,
Nombre varchar(100),
ApellidoPaterno varchar(100),
ApellidoMaterno varchar(100),
Contraseņa varchar(100),
FkIdPrivilegio int,
foreign key(FkIdPrivilegio) references Privilegios(IdPrivilegio));

describe Privilegios;
describe Alarma;
describe Usuarios;