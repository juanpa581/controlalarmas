﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatosAlarma;
using EntidadesAlarma;
using System.Windows.Forms;


namespace ManejadoresAlarma
{
    public class ManejadorPrivilegios
    {
        ConexionBD cbd = new ConexionBD();

        public string Guardar(Privilegios privilegios)
        {
            return cbd.Comando(string.Format("insert into Privilegios values(" +
                "null,'{0}')", privilegios.Privilegio));
        }
        public string Borrar(Privilegios privilegios)
        {
            return cbd.Comando(string.Format("delete from Privilegios where IdPrivilegio = {0}", privilegios.IdPrivilegio));
        }
        public string Modificar(Privilegios privilegios)
        {
            return cbd.Comando(string.Format("update Privilegios set Privilegio = '{0}' where IdPrivilegio = {1}", privilegios.Privilegio, privilegios.IdPrivilegio));
        }
        public DataSet Listado(string q, string tabla)
        {
            return cbd.Mostrar(q, tabla);
        }
    }
}
