﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatosAlarma;
using EntidadesAlarma;
using System.Windows.Forms;

namespace ManejadoresAlarma
{
    public class ManejadorAlarma
    {
        ConexionBD cbd = new ConexionBD();

        public string Guardar(Alarma alarma)
        {
            return cbd.Comando(string.Format("insert into Alarma values(" +
                "null,'{0}')", alarma.Estado));
        }
        public string Borrar(Alarma alarma)
        {
            return cbd.Comando(string.Format("delete from Alarma where IdAlarma = {0}", alarma.IdAlarma));
        }
        public string Modificar(Alarma alarma)
        {
            return cbd.Comando(string.Format("update Alarma set Estado = '{0}' where IdAlarma = {1}", alarma.Estado, alarma.IdAlarma));
        }
        public DataSet Listado(string q, string tabla)
        {
            return cbd.Mostrar(q, tabla);
        }
    }
}
