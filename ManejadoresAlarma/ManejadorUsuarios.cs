﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatosAlarma;
using EntidadesAlarma;
using System.Windows.Forms;

namespace ManejadoresAlarma
{
    public class ManejadorUsuarios
    {
        ConexionBD cbd = new ConexionBD();
        public string Guardar(Usuarios usuarios)
        {
            return cbd.Comando(string.Format("insert into Usuarios values(" +
                "null,'{0}', '{1}', '{2}', md5('{3}'), {4})", usuarios.Nombre, usuarios.ApellidoPaterno, usuarios.ApellidoMaterno, usuarios.Contraseña, usuarios.FkIdPrivilegio));
        }
        public string Borrar(Usuarios usuarios)
        {
            return cbd.Comando(string.Format("delete from Usuarios where IdUsuario={0}", usuarios.IdUsuario));
        }
        public string Modificar(Usuarios usuarios)
        {
            return cbd.Comando(string.Format("update Usuarios set Nombre = '{0}', ApellidoPaterno = '{1}',  ApellidoMaterno = '{2}',  Contraseña = md5('{3}'), FkIdPrivilegio = {4}  where IdUsuario = {5}", usuarios.Nombre, usuarios.ApellidoPaterno, usuarios.ApellidoMaterno, usuarios.Contraseña, usuarios.FkIdPrivilegio, usuarios.IdUsuario));
        }
        public DataSet Listado(string q, string tabla)
        {
            return cbd.Mostrar(q, tabla);
        }
    }
}
