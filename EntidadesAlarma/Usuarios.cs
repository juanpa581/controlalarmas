﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAlarma
{
    public class Usuarios
    {
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Contraseña { get; set; }
        public int FkIdPrivilegio { get; set; }
        public Usuarios(int idusuario, string nombre, string apellidopaterno, string apellidomaterno, string contraseña, int fkidprivilegio)
        {
            IdUsuario = idusuario;
            Nombre = nombre;
            ApellidoPaterno = apellidopaterno;
            ApellidoMaterno = apellidomaterno;
            Contraseña = contraseña;
            FkIdPrivilegio = fkidprivilegio;
        }
    }
}