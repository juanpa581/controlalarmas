﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAlarma
{
    public class Alarma
    {
        public int IdAlarma { get; set; }
        public string Estado { get; set; }
        public Alarma(int idalarma, string estado)
        {
            IdAlarma = idalarma;
            Estado = estado;
        }
    }
}
