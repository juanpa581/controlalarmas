﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAlarma
{
    public class Privilegios
    {
        public int IdPrivilegio { get; set; }
        public string Privilegio { get; set; }
        public Privilegios(int idprivilegio, string privilegio)
        {
            IdPrivilegio = idprivilegio;
            Privilegio = privilegio;
        }

    }
}
